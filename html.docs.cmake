message(WARNING "Generating HTML docs at the folder ${PROJECT_SOURCE_DIR}/docs")
add_custom_target(
    htmldocs
    )
add_custom_command(
    TARGET htmldocs
    COMMAND "/usr/bin/make" "clean"
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}/docs"
    )
add_custom_command(
    TARGET htmldocs
    COMMAND "/usr/bin/doxygen"
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}/docs"
    )
add_custom_command(
    TARGET htmldocs
    COMMAND "/usr/bin/make" "html"
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}/docs"
    )
message(STATUS "Errors:" ${CMD_ERROR})

